using Microsoft.AspNetCore.Mvc;

namespace Ets.Identity.Controllers
{
    [Produces("application/json")]
    [ProducesResponseType(200)]
    [ProducesResponseType(202)]
    [ProducesResponseType(204)]
    [ProducesResponseType(400)]
    [ProducesResponseType(422)]
    [ProducesResponseType(500)]
    [ApiController]
    public class ApiControllerBase : ControllerBase
    {
        
    }
}