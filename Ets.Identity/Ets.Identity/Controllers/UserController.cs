using System;
using System.Threading.Tasks;
using Amethyst.Microservices.WebApi.Jwt;
using Ets.Identity.Models.InputModels;
using Ets.Identity.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ets.Identity.Controllers
{
    [Route("/api/user")]
    public sealed class UserController : ApiControllerBase
    {
        private readonly IUserService _service;

        public UserController(IUserService service)
            =>  _service = service;

        /// <summary>
        ///     Сменить пароль
        /// </summary>
        [HttpPatch("password")]
        [Authorize(Roles = "Customer, Admin")]
        public async Task<IActionResult> ChangeCustomerPassword(ChangePasswordInputModel inputModel)
        {
            var isSuccess = Request.TryGetUserId(out var userId);
            if (!isSuccess)
                return UnprocessableEntity("Invalid JWT payload");

            var operationResult =  await _service.ChangePasswordAsync(userId, inputModel);
            if (!operationResult.IsSuccess)
                return UnprocessableEntity(operationResult.Error);

            return Ok();
        }
        
        /// <summary>
        ///     Сменить роль
        /// </summary>
        [HttpPatch("{userId:guid}/role")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ChangeRole(Guid userId, ChangeRoleInputModel inputModel)
        {
            if (userId.Equals(Guid.Empty))
                return BadRequest("User id not specified.");
            
            var operationResult = await _service.ChangeRoleAsync(userId, inputModel);
            if (!operationResult.IsSuccess)
                return UnprocessableEntity(operationResult.Error);

            return Ok();
        }
        
        /// <summary>
        ///     Сменить имя пользоватаеля
        /// </summary>
        [HttpPatch("{userId:guid}/username")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ChangeUsername(Guid userId, ChangeUsernameInputModel inputModel)
        {
            if (userId.Equals(Guid.Empty))
                return UnprocessableEntity("User id not specified.");
            
            var operationResult = await _service.ChangeUsernameAsync(userId, inputModel);
            if (!operationResult.IsSuccess)
                return UnprocessableEntity(operationResult.Error);

            return Ok();
        }
        
        /// <summary>
        ///     Сменить email
        /// </summary>
        [HttpPatch("{userId:guid}/email")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> ChangeEmail(Guid userId, ChangeEmailInputModel inputModel)
        {
            if (userId.Equals(Guid.Empty))
                return UnprocessableEntity("User id not specified.");
            
            var operationResult = await _service.ChangeEmailAsync(userId, inputModel);
            if (!operationResult.IsSuccess)
                return UnprocessableEntity(operationResult.Error);

            return Ok();
        }
        
        /// <summary>
        ///     Заблокировать пользователя
        /// </summary>
        [HttpPatch("{userId:guid}/block")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Block(Guid userId)
        {
            if (userId.Equals(Guid.Empty))
                return UnprocessableEntity("User id not specified.");
            
            var operationResult = await _service.BlockAsync(userId);
            if (!operationResult.IsSuccess)
                return UnprocessableEntity(operationResult.Error);

            return Ok();
        }
        
        /// <summary>
        ///     Активировать пользователя
        /// </summary>
        [HttpPatch("{userId:guid}/activate")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Activate(Guid userId)
        {
            if (userId.Equals(Guid.Empty))
                return UnprocessableEntity("User id not specified.");
            
            var operationResult = await _service.ActivateAsync(userId);
            if (!operationResult.IsSuccess)
                return UnprocessableEntity(operationResult.Error);

            return Ok();
        }
        
        /// <summary>
        ///     Удалить пользователя
        /// </summary>
        [HttpDelete("{userId:guid}/remove")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Remove(Guid userId)
        {
            if (userId.Equals(Guid.Empty))
                return UnprocessableEntity("User id not specified.");
            
            var operationResult = await _service.RemoveAsync(userId);
            if (!operationResult.IsSuccess)
                return UnprocessableEntity(operationResult.Error);

            return Ok();
        }
    }
}