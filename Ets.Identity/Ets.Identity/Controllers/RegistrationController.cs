using System.Threading.Tasks;
using Amethyst.Microservices.WebApi.Configurations;
using Ets.Identity.Infrastructure.Helpers;
using Ets.Identity.Models.InputModels;
using Ets.Identity.Models.ViewModels;
using Ets.Identity.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SharpJuice.Essentials;

namespace Ets.Identity.Controllers
{
    [Route("/api/registration")]
    [Authorize(Roles = "Admin")]
    public class RegistrationController : ApiControllerBase
    {
        private readonly IRegistrationService _service;
        private readonly JwtConfiguration _jwtConfiguration;
        private readonly IClock _clock;
        
        public RegistrationController(
            IRegistrationService service, 
            IOptions<JwtConfiguration> jwtConfiguration, 
            IClock clock)
        {
            _service = service;
            _jwtConfiguration = jwtConfiguration.Value;
            _clock = clock;
        }

        /// <summary>
        ///     Создать пользователя
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<CompleteToken>> CreateUser(CreateUserInputModel inputModel)
        {
            var operationResult = await _service.CreateUserAsync(inputModel);
            if (!operationResult.IsSuccess)
                return UnprocessableEntity(operationResult.Error);

            var token = operationResult.User.CreateToken(
                operationResult.Roles, 
                _jwtConfiguration, 
                _clock.Now);

            return Ok(token);
        }
    }
}