using System.Threading.Tasks;
using Amethyst.Microservices.WebApi.Configurations;
using Amethyst.Microservices.WebApi.Jwt;
using Ets.Identity.Infrastructure.Helpers;
using Ets.Identity.Models.InputModels;
using Ets.Identity.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SharpJuice.Essentials;
using IAuthorizationService = Ets.Identity.Services.Contracts.IAuthorizationService;

namespace Ets.Identity.Controllers
{
    [Route("/api/auth")]
    public sealed class AuthorizationController : ApiControllerBase
    {
        private readonly IAuthorizationService _authService;
        private readonly JwtConfiguration _jwtConfiguration;
        private readonly IClock _clock;
        
        public AuthorizationController(
            IAuthorizationService authService, 
            IClock clock, 
            IOptions<JwtConfiguration> options)
        {
            _authService = authService;
            _jwtConfiguration = options.Value;
            _clock = clock;
        }

        /// <summary>
        ///     Авторизовать пользователя
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<CompleteToken>> Auth(AuthInputModel inputModel)
        {
            var operationResult = await _authService.AuthAsync(inputModel);
            if (!operationResult.IsSuccess || operationResult.User == null)
                return UnprocessableEntity(operationResult.Error);

            var token = operationResult.User.CreateToken(
                operationResult.Roles, 
                _jwtConfiguration, 
                _clock.Now);

            return Ok(token);
        }
        
        /// <summary>
        ///     Продлить сессию пользователя
        /// </summary>
        [HttpPost("refresh")]
        [Authorize(Roles = "Refresh")]
        public async Task<ActionResult<CompleteToken>> Refresh()
        {
            var isSuccess = Request.TryGetUserId(out var userId);
            if (!isSuccess)
                return BadRequest("Invalid JWT payload");

            var operationResult = await _authService.RefreshSessionAsync(userId);
            if (!operationResult.IsSuccess || operationResult.User == null)
                return UnprocessableEntity(operationResult.Error);

            var token = operationResult.User.CreateToken(
                operationResult.Roles, 
                _jwtConfiguration, 
                _clock.Now);

            return Ok(token);
        }
    }
}