﻿using Amethyst.Platform;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Ets.Identity
{
    public class Program
    {
        public static void Main(string[] args)
            => CreateWebHostBuilder(args)
                .Build()
                .Run();

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureServices(services => services.AddAutofac())
                .UsePlatform()
                .UseStartup<Startup>();
    }
}