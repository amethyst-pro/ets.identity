﻿using System;
using Amethyst.Microservices.WebApi;
using Amethyst.Notifications.Pgsql.Extensions;
using AutoMapper;
using Ets.Identity.Dal;
using Ets.Identity.Dal.Extensions;
using Ets.Identity.Infrastructure.Extensions;
using Ets.Identity.Models.Entities;
using Ets.Identity.Services.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ets.Identity
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
            => _configuration = configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            var postgresConnectionString = _configuration.GetConnectionString("ReadModels");
            var postgresNotifications = _configuration.GetSection("PgsqlNotifications");
            
            services.AddPgsqlNotifications(postgresNotifications, postgresConnectionString);
            services.AddServices();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddContexts();
            services.AddIdentity();
            services.AddClock();
            services.AddDal(_configuration);
            services.AddHttpContextAccessor();
            services.AddGrpcClients(_configuration);
            services.AddJwtAuthentication(_configuration);
            services.AddCorsPolicy(_configuration);
            services.ConfigureSwagger(_configuration);
            services.AddMvcConfiguration();
        }
        
        public void Configure(
            IApplicationBuilder app, 
            IHostingEnvironment env, 
            UserManager<DomainUser> userManager, 
            RoleManager<DomainRole> roleManager)
        {
            IdentityInitializer.SeedData(userManager, roleManager);
            
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseAuthentication();
            app.UseRpcExtensionHandler();
            app.UseExceptionHandlers();
            app.UseStaticFiles();
            app.UseCors("CorsPolicy");
            app.UseMvc();
        }
    }
}