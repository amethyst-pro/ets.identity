using System.Text.RegularExpressions;
using Ets.Identity.Models.InputModels;
using FluentValidation;

namespace Ets.Identity.Validation
{
    public sealed class ChangeUsernameInputModelValidator : AbstractValidator<ChangeUsernameInputModel>
    {
        public ChangeUsernameInputModelValidator()
        {
            RuleFor(m => m.Username)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("Поле {PropertyName} не заполнено.")
                .Length(4, 15)
                .WithMessage("Поле {PropertyName} должно содержать от 4 до 15 символов.")
                .Matches(new Regex("^[A-Za-z0-9]*$"))
                .WithMessage("Поле {PropertyName} должно содержать только латинские символы и цифры.");
        }
    }
}