using Ets.Identity.Models.InputModels;
using FluentValidation;

namespace Ets.Identity.Validation
{
    public sealed class ChangeRoleInputModelValidator : AbstractValidator<ChangeRoleInputModel>
    {
        public ChangeRoleInputModelValidator()
        {
            RuleFor(m => m.Role)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("Поле {PropertyName} не заполнено.")
                .IsInEnum()
                .WithMessage("Поле {PropertyName} заполнено некорректно.");
        }
    }
}