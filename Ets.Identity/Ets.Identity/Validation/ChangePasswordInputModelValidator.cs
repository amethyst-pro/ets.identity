using System.Text.RegularExpressions;
using Ets.Identity.Models.InputModels;
using FluentValidation;

namespace Ets.Identity.Validation
{
    public sealed class ChangePasswordInputModelValidator : AbstractValidator<ChangePasswordInputModel>
    {
        private readonly Regex _pattern = 
            new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,}$");
        
        public ChangePasswordInputModelValidator()
        {
            RuleFor(m => m.CurrentPassword)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("Поле {PropertyName} не заполнено.")
                .Matches(_pattern)
                .WithMessage("Поле {PropertyName} заполнено некорректно.");
            
            RuleFor(m => m.NewPassword)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("Поле {PropertyName} не заполнено.")
                .Matches(_pattern)
                .WithMessage("Поле {PropertyName} заполнено некорректно.");
            
            RuleFor(m => m.ConfirmNewPassword)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("Поле {PropertyName} не заполнено.")
                .Matches(_pattern)
                .WithMessage("Поле {PropertyName} заполнено некорректно.");
            
            RuleFor(x => x).Custom((x, context) =>
            {
                if (x.NewPassword != x.ConfirmNewPassword)
                    context.AddFailure(nameof(x.CurrentPassword), "Пароли должны совпадать");
            });
        }
    }
}