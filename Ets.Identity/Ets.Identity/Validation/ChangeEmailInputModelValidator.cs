using Ets.Identity.Models.InputModels;
using FluentValidation;

namespace Ets.Identity.Validation
{
    public sealed class ChangeEmailInputModelValidator : AbstractValidator<ChangeEmailInputModel>
    {
        public ChangeEmailInputModelValidator()
        {
            RuleFor(m => m.Email)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("Поле {PropertyName} не заполнено.")
                .EmailAddress()
                .WithMessage("Поле {PropertyName} заполнено некорректно.");
        }
    }
}