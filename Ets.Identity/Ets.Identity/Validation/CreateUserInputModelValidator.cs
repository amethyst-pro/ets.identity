using System.Text.RegularExpressions;
using Ets.Identity.Models.InputModels;
using FluentValidation;

namespace Ets.Identity.Validation
{
    public sealed class CreateUserInputModelValidator : AbstractValidator<CreateUserInputModel>
    {
        private readonly Regex _pattern = 
            new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,}$");
        
        public CreateUserInputModelValidator()
        {
            RuleFor(m => m.Username)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("Поле {PropertyName} не заполнено.")
                .Length(4, 15)
                .WithMessage("Поле {PropertyName} должно содержать от 4 до 15 символов.")
                .Matches(new Regex("^[A-Za-z0-9]*$"))
                .WithMessage("Поле {PropertyName} должно содержать только латинские символы и цифры.");

            RuleFor(m => m.Role)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("Поле {PropertyName} не заполнено.")
                .IsInEnum()
                .WithMessage("Поле {PropertyName} заполнено некорректно.");
            
            RuleFor(m => m.Email)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("Поле {PropertyName} не заполнено.")
                .EmailAddress()
                .WithMessage("Поле {PropertyName} заполнено некорректно.");
            
            RuleFor(m => m.Password)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("Поле {PropertyName} не заполнено.")
                .Matches(_pattern)
                .WithMessage("Поле {PropertyName} заполнено некорректно.");
            
            RuleFor(m => m.ConfirmPassword)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .WithMessage("Поле {PropertyName} не заполнено.")
                .Matches(_pattern)
                .WithMessage("Поле {PropertyName} заполнено некорректно.");
            
            RuleFor(x => x).Custom((x, context) =>
            {
                if (x.Password != x.ConfirmPassword)
                    context.AddFailure(nameof(x.Password), "Пароли должны совпадать");
            });
        }
    }
}