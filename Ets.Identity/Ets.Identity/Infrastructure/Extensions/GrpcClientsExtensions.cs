using Amethyst.Grpc.Extensions;
using Amethyst.Microservices.WebApi.Configurations;
using Amethyst.Microservices.WebApi.Interceptors;
using Amethyst.Microservices.WebApi.Middlewares;
using Amethyst.Platform.Grpc.Extensions;
using Ets.Services.Users.Grpc;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ets.Identity.Infrastructure.Extensions
{
    public static class GrpcClientsExtensions
    {
        public static IServiceCollection AddGrpcClients(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddPlatformGrpc();
            services.AddTransient<GrpcInterceptor>();

            services.AddGrpcChannel(channel =>
            {
                var config = configuration.GetSection("UsersService").Get<GrpcClientConfiguration>();
                channel.Bind(config.Host, config.Port);
                channel.AddInterceptor<GrpcInterceptor>();
           
                channel.AddClient<UserGrpc.UserGrpcClient>();
                channel.AddClient<ProfileGrpc.ProfileGrpcClient>();
            });
            
            return services;
        }
        
        public static void UseRpcExtensionHandler(this IApplicationBuilder app)
            => app.UseMiddleware<RpcExceptionMiddleware>();
    }
}