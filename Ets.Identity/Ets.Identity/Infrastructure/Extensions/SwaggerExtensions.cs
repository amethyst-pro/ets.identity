using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Ets.Identity.Infrastructure.Extensions
{
    public static class SwaggerExtensions
    {
        public static void ConfigureSwagger(this IServiceCollection services, IConfiguration configuration)
            => services.ConfigureSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.DescribeAllParametersInCamelCase();
                options.AddAuthorization();
                options.AddCopirights();
                options.MapType<long[]>(() => new Schema
                {
                    Type = "array",
                    Items = new Schema {Type = "integer"}, 
                    Format = "string"
                });
            });

        private static void AddAuthorization(this SwaggerGenOptions options)
        {

            options.AddSecurityDefinition("Bearer", new ApiKeyScheme
            {
                Description = "JWT авторизация: \"Bearer {token}\"",
                Name = "Authorization",
                In = "header",
                Type = "apiKey"
            });

            var security = new Dictionary<string, IEnumerable<string>>
            {
                {"Bearer", new string[] { }}
            };
            options.AddSecurityRequirement(security);
        }

        private static void AddCopirights(this SwaggerGenOptions options)
        {
            options.SwaggerDoc("v1", new Info
            {
                Version = "v1",
                Title = "ETS Identity API",
                Description = "ETS Identity server",
                Contact = new Contact
                {
                    Name = "Boris B. Goldovsky",
                    Email = "setsunsoul@outlook.com"
                },
                License = new License
                {
                    Name = "Use under GNU v.3",
                    Url = "https://www.gnu.org/licenses/gpl-3.0.ru.html"
                }
            });
        }

        public static IApplicationBuilder UseSwaggerConfiguration(this IApplicationBuilder app)
            => app.UseSwagger()
                .UseSwaggerUI(c =>c.SwaggerEndpoint("/swagger/v1/swagger.json", "ETS IDENTITY API V1"));
        }
}