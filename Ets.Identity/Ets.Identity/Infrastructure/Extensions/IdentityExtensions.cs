using System;
using Ets.Identity.Dal;
using Ets.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Ets.Identity.Infrastructure.Extensions
{
    public static class IdentityExtensions
    {
        public static IServiceCollection AddIdentity(this IServiceCollection services)
        {
            services
                .AddIdentity<DomainUser, DomainRole>(opt => opt.User.RequireUniqueEmail = true)
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders()
                .AddUserStore<UserStore<DomainUser, DomainRole, AppDbContext, Guid>>()
                .AddRoleStore<RoleStore<DomainRole, AppDbContext, Guid>>();

            return services;
        }
    }
}