using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Amethyst.Microservices.WebApi.Configurations;
using Ets.Identity.Models.Entities;
using Ets.Identity.Models.Primitives;
using Ets.Identity.Models.ViewModels;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.IdentityModel.Tokens;

namespace Ets.Identity.Infrastructure.Helpers
{
    public static class JwtFactory
    {            
        public static CompleteToken CreateToken(
            this DomainUser user, 
            IList<string> roles,
            JwtConfiguration configuration, 
            DateTimeOffset creationDate)
        {
            var accessExpires = creationDate.AddMinutes(configuration.AccessExpiresMinutes);
            var accessToken = BuildToken(user, roles, JwtType.Access, configuration, accessExpires);
            
            var refreshExpires = creationDate.AddMinutes(configuration.RefreshExpiresMinutes);
            var refreshToken = BuildToken(user, roles, JwtType.Refresh, configuration, refreshExpires);
            
            return new CompleteToken(accessToken, refreshToken);
        }

        private static string BuildToken(
            DomainUser user, 
            IEnumerable<string> roles,
            JwtType type,
            JwtConfiguration configuration, 
            DateTimeOffset expires)
        {
            const string refreshRole = "Refresh";
            
            var jwtRoles = type == JwtType.Access
                ? roles.Join()
                : refreshRole;
            
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Role, jwtRoles)
            };

            var symmetricSecurity = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.SecurityKey));
            var signingCredentials = new SigningCredentials(symmetricSecurity, SecurityAlgorithms.HmacSha256);
            
            var jwtSecurityToken = new JwtSecurityToken(configuration.Issuer, 
                configuration.Issuer, 
                claims,
                expires: expires.DateTime, 
                signingCredentials: signingCredentials);
            
            return new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        }
    }
}