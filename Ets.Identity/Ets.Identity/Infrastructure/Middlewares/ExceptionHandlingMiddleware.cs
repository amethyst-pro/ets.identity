using System.Net;
using System.Threading.Tasks;
using Amethyst.Notifications.Abstractions.Exceptions;
using Ets.Identity.Services.Exceptions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Ets.Identity.Infrastructure.Middlewares
{
    public sealed class ExceptionHandlingMiddleware
    {
        private const string ContentType = "application/json";
        private readonly RequestDelegate _next;
        
        public ExceptionHandlingMiddleware(RequestDelegate next)
            => _next = next;

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (NotificationTimeoutException e)
            {
                context.Response.StatusCode = (int) HttpStatusCode.Accepted;
                context.Response.ContentType = ContentType;
                
                var messages = new {Messages = new[] { e.Message }};
                var serializedError = JsonConvert.SerializeObject(messages);
                
                await context.Response.WriteAsync(serializedError);
            }
            catch (InternalValidationException e)
            {
                context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
                context.Response.ContentType = ContentType;
                
                var errors = new {Errors = new { Validation = e.Message }};
                var serializedError = JsonConvert.SerializeObject(errors);
                
                await context.Response.WriteAsync(serializedError);
            }
        }
    }
}