using System;
using Ets.Identity.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Ets.Identity.Dal
{
    public sealed class AppDbContext : IdentityDbContext<DomainUser, DomainRole, Guid>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
            => Database.EnsureCreated();
    }
}