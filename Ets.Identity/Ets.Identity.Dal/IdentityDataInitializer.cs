using System;
using Ets.Identity.Models.Entities;
using Ets.Identity.Models.Primitives;
using Microsoft.AspNetCore.Identity;

namespace Ets.Identity.Dal
{
    public static class IdentityInitializer
    {
        private const string Admin = "Admin";
        private const string Customer = "Customer";
            
        public static void SeedData(UserManager<DomainUser> userManager, RoleManager<DomainRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }

        private static void SeedUsers(UserManager<DomainUser> userManager)
        {
            const string username = "SuperAdmin";
            const string password = "SuperAdmin123!";

            if (userManager.FindByNameAsync(username).Result != null) 
                return;

            var user = new DomainUser 
            {
                UserName = username, 
                Email = "user1@localhost",
                IsActive = true
            };

            var creationResult = userManager.CreateAsync(user, password).Result;
            
            if (!creationResult.Succeeded)
                throw new InvalidOperationException("Can't create default administrator.");
            
            var code = userManager.GenerateEmailConfirmationTokenAsync(user).Result;
            
            var confirmationResult = userManager.ConfirmEmailAsync(user, code).Result;
            if (!confirmationResult.Succeeded)
                throw new InvalidOperationException("Can't activate default administrator.");

            var assignResult = userManager.AddToRoleAsync(user, Admin).Result;
            if (!assignResult.Succeeded)
                throw new InvalidOperationException("Can't assign default administrator role.");
        }

        private static void SeedRoles(RoleManager<DomainRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync(Admin).Result)
            {
                var role = new DomainRole
                {
                    Name = Admin, 
                    Description = "Perform all operations.", 
                    Value = UserRole.Admin
                };

                var result = roleManager.CreateAsync(role).Result;
                if (!result.Succeeded)
                    throw new InvalidOperationException($"Can't create {Admin} role.");
            }
            
            if (!roleManager.RoleExistsAsync(Customer).Result)
            {
                var role = new DomainRole
                {
                    Name = Customer, 
                    Description = "Perform customer operations.",
                    Value = UserRole.Customer
                };
                    
                var result = roleManager.CreateAsync(role).Result;
                if (!result.Succeeded)
                    throw new InvalidOperationException($"Can't create {Customer} role.");
            }
        }
    }
}