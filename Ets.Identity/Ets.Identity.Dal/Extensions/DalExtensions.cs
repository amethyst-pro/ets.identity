using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ets.Identity.Dal.Extensions
{
    public static class DalExtensions
    {
        public static IServiceCollection AddDal(this IServiceCollection services, IConfiguration configuration)
            => services.AddDbContext<AppDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("Identity"),  
                    b => b.MigrationsAssembly("Ets.Identity")));
    }
}