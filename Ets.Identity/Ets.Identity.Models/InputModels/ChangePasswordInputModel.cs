namespace Ets.Identity.Models.InputModels
{
    public sealed class ChangePasswordInputModel
    {
        public string CurrentPassword { get; set; }
        
        public string NewPassword { get; set; }
        
        public string ConfirmNewPassword { get; set; }
    }
}