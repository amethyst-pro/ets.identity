namespace Ets.Identity.Models.InputModels
{
    public sealed class AuthInputModel
    {
        public string Username { get; set; }
        
        public string Password { get; set; }
        
        public bool RememberMe { get; set; }
    }
}