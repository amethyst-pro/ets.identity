namespace Ets.Identity.Models.InputModels
{
    public class ChangeUsernameInputModel
    {
        public string Username { get; set; }
    }
}