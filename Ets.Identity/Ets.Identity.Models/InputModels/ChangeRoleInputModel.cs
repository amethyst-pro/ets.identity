using Ets.Identity.Models.Primitives;

namespace Ets.Identity.Models.InputModels
{
    public sealed class ChangeRoleInputModel
    {
        public UserRole Role { get; set; }
    }
}