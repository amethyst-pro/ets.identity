namespace Ets.Identity.Models.InputModels
{
    public sealed class ChangeEmailInputModel
    {
        public string Email { get; set; }
    }
}