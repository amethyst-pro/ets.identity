using Ets.Identity.Models.Primitives;

namespace Ets.Identity.Models.InputModels
{
    public sealed class CreateUserInputModel
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public UserRole Role => UserRole.Customer;
        
        public string Password { get; set; }
        
        public string ConfirmPassword { get; set; }
    }
}