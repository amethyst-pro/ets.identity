namespace Ets.Identity.Models.Primitives
{
    public enum JwtType
    {
        None = 0,
        Access,
        Refresh
    }
}