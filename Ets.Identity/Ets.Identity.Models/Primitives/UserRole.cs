namespace Ets.Identity.Models.Primitives
{
    public enum UserRole
    {
        None = 0,
        Admin,
        Customer
    }
}