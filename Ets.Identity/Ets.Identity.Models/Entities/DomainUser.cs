using System;
using Microsoft.AspNetCore.Identity;

namespace Ets.Identity.Models.Entities
{
    public sealed class DomainUser : IdentityUser<Guid>
    {
        public bool IsActive { get; set; }
    }
}