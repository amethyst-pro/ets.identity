using System;
using Ets.Identity.Models.Primitives;
using Microsoft.AspNetCore.Identity;

namespace Ets.Identity.Models.Entities
{
    public sealed class DomainRole : IdentityRole<Guid>
    {
        public UserRole Value { get; set; }
        
        public string Description { get; set; }
    }
}