using System.Collections.Generic;
using Ets.Identity.Models.Entities;

namespace Ets.Identity.Models.ViewModels
{
    public readonly struct OperationResult
    {
        private OperationResult(DomainUser user, IList<string> roles, bool isSuccess, string error)
        {
            User = user;
            Roles = roles;
            IsSuccess = isSuccess;
            Error = error;
        }

        public static OperationResult Success(DomainUser user, IList<string> roles)
            => new OperationResult(user, roles, true, default);
        
        public static OperationResult Fail(string error)
            => new OperationResult(default, default, default, error);

        public DomainUser User { get; }
        
        public IList<string> Roles { get; }
        
        public bool IsSuccess { get; }
        
        public string Error { get; }
    }
}