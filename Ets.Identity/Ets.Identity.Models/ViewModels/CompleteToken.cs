using System;

namespace Ets.Identity.Models.ViewModels
{
    public readonly struct CompleteToken : IEquatable<CompleteToken>
    {
        public CompleteToken(string access, string refresh)
        {
            Access = access;
            Refresh = refresh;
        }
        
        public string Access { get; }
        
        public string Refresh { get; }

        public bool Equals(CompleteToken other)
            => string.Equals(Access, other.Access) && string.Equals(Refresh, other.Refresh);

        public override bool Equals(object obj)
            => obj is CompleteToken other && Equals(other);

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Access != null ? Access.GetHashCode() : 0) * 397) ^ 
                       (Refresh != null ? Refresh.GetHashCode() : 0);
            }
        }
    }
}