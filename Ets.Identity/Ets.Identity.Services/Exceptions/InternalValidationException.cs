using System;

namespace Ets.Identity.Services.Exceptions
{
    public sealed class InternalValidationException : Exception
    {
        public InternalValidationException(string message)
            : base($"Validation error. {message}.")
        {
        }
    }
}