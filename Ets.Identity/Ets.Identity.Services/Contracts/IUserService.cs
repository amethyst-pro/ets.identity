using System;
using System.Threading.Tasks;
using Ets.Identity.Models.InputModels;
using Ets.Identity.Models.ViewModels;

namespace Ets.Identity.Services.Contracts
{
    public interface IUserService
    {
        Task<OperationResult> ChangePasswordAsync(Guid userId, ChangePasswordInputModel inputModel);
        
        Task<OperationResult> ChangeRoleAsync(Guid userId, ChangeRoleInputModel inputModel);

        Task<OperationResult> ChangeUsernameAsync(Guid userId, ChangeUsernameInputModel inputModel);

        Task<OperationResult> ChangeEmailAsync(Guid userId, ChangeEmailInputModel inputModel);

        Task<OperationResult> ActivateAsync(Guid userId);
        
        Task<OperationResult> BlockAsync(Guid userId);
        
        Task<OperationResult> RemoveAsync(Guid userId);
    }
}