using System;
using System.Threading.Tasks;
using Ets.Identity.Models.InputModels;
using Ets.Identity.Models.ViewModels;

namespace Ets.Identity.Services.Contracts
{
    public interface IAuthorizationService
    {
        Task<OperationResult> AuthAsync(AuthInputModel inputModel);
        
        Task<OperationResult> RefreshSessionAsync(Guid userId);
    }
}