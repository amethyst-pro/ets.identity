using System.Threading.Tasks;
using Ets.Identity.Models.InputModels;
using Ets.Identity.Models.ViewModels;

namespace Ets.Identity.Services.Contracts
{
    public interface IRegistrationService
    {
        Task<OperationResult> CreateUserAsync(CreateUserInputModel inputModel);
    }
}