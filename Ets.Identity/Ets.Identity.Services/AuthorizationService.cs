using System;
using System.Linq;
using System.Threading.Tasks;
using Ets.Identity.Models.Entities;
using Ets.Identity.Models.InputModels;
using Ets.Identity.Models.ViewModels;
using Ets.Identity.Services.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Ets.Identity.Services
{
    public sealed class AuthorizationService : IAuthorizationService
    {
        private readonly ILogger<AuthorizationService> _logger;
        private readonly UserManager<DomainUser> _userManager;
        private readonly SignInManager<DomainUser> _signInManager;

        public AuthorizationService(
            ILogger<AuthorizationService> logger, 
            UserManager<DomainUser> userManager, 
            SignInManager<DomainUser> signInManager)
        {
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        
        public async Task<OperationResult> AuthAsync(AuthInputModel inputModel)
        {
            var user = await _userManager.FindByNameAsync(inputModel.Username);
            if (user == null)
                return OperationResult.Fail($"User ({inputModel.Username}) not found.");
            
            if (!user.IsActive)
                return OperationResult.Fail($"User ({inputModel.Username}) blocked.");
            
            var roles = await _userManager.GetRolesAsync(user);
            if (roles == null || !roles.Any())
                return OperationResult.Fail($"Roles for user ({user.Id}) not found");

            var signInResult = await _signInManager.PasswordSignInAsync(
                inputModel.Username, 
                inputModel.Password, 
                inputModel.RememberMe, 
                false);
            
            if (!signInResult.Succeeded) 
                return OperationResult.Fail("Wrong username or password.");

            _logger.LogDebug($"User ({user.Id}) authorized.");

            return OperationResult.Success(user, roles);
        }

        public async Task<OperationResult> RefreshSessionAsync(Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
                return OperationResult.Fail($"User ({userId}) not found.");
            
            if (!user.IsActive)
                return OperationResult.Fail($"User ({userId}) blocked.");

            var roles = await _userManager.GetRolesAsync(user);
            if (roles == null || !roles.Any())
                return OperationResult.Fail($"Roles for user ({userId}) not found.");

            await _signInManager.RefreshSignInAsync(user);
            
            _logger.LogDebug($"User ({userId}) session refreshed.");
            
            return OperationResult.Success(user, roles);
        }
    }
}