using System;
using System.Threading.Tasks;
using Amethyst.Notifications.Abstractions;
using Ets.Identity.Models.Entities;
using Ets.Identity.Models.InputModels;
using Ets.Identity.Models.ViewModels;
using Ets.Identity.Services.Contracts;
using Ets.Identity.Services.Extensions;
using Ets.Services.Users.Grpc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;

namespace Ets.Identity.Services
{
    public sealed class UserService : IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly UserManager<DomainUser> _userManager;
        private readonly SignInManager<DomainUser> _signInManager;
        private readonly ProfileGrpc.ProfileGrpcClient _profileClient;
        private readonly UserGrpc.UserGrpcClient _userClient;
        private readonly INotificationsChannel _notificationsChannel;
        private readonly INotificationContext _notificationContext;
        private readonly TimeSpan _timeout = TimeSpan.FromSeconds(60);

        public UserService(
            ILogger<UserService> logger, 
            UserManager<DomainUser> userManager,
            SignInManager<DomainUser> signInManager, 
            ProfileGrpc.ProfileGrpcClient profileClient, 
            UserGrpc.UserGrpcClient userClient, 
            INotificationsChannel notificationsChannel, 
            INotificationContext notificationContext)
        {
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
            _profileClient = profileClient;
            _userClient = userClient;
            _notificationsChannel = notificationsChannel;
            _notificationContext = notificationContext;
        }
        
        private const string DefaultAdminName = "SuperAdmin";

        public async Task<OperationResult> ChangePasswordAsync(Guid userId, ChangePasswordInputModel inputModel)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
                return OperationResult.Fail($"User ({userId}) not found.");
            
            if (!user.IsActive)
                return OperationResult.Fail($"User ({userId}) blocked.");

            var roles = await _userManager.GetRolesAsync(user);
            if (roles == null || !roles.Any())
                return OperationResult.Fail($"Roles for user ({userId}) not found.");

            var identityResult = await _userManager.ChangePasswordAsync(user, inputModel.CurrentPassword, inputModel.NewPassword);
            if (!identityResult.Succeeded)
                return OperationResult.Fail(identityResult.GetErrors());
            
            await _signInManager.RefreshSignInAsync(user);

            _logger.LogDebug($"Password for user ({user.Id}) changed.");
            
            return OperationResult.Success(user, roles);
        }

        public async Task<OperationResult> ChangeRoleAsync(Guid userId, ChangeRoleInputModel inputModel)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
                return OperationResult.Fail($"User ({userId}) not found.");
            
            if (!user.IsActive)
                return OperationResult.Fail($"User ({userId}) blocked.");

            if (user.UserName == DefaultAdminName)
                return OperationResult.Fail("Can't change super admin role.");
            
            var roles = await _userManager.GetRolesAsync(user);
            if (roles == null || !roles.Any())
                return OperationResult.Fail($"Roles for user ({userId}) not found.");

            var removeResult = await _userManager.RemoveFromRolesAsync(user, roles);
            if (!removeResult.Succeeded)
                return OperationResult.Fail(removeResult.GetErrors());
            
            var addResult = await _userManager.AddToRoleAsync(user, inputModel.Role.ToString());
            if (!addResult.Succeeded)
                return OperationResult.Fail(addResult.GetErrors());

            await _signInManager.RefreshSignInAsync(user);
            
            var srvRequest = new ChangeRoleRequest
            {
                UserId = userId.ToProto(),
                Role = (RoleProto)inputModel.Role
            };
            
            using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
            {
                var srvResult = await _userClient.ChangeRoleAsync(srvRequest);
                
                if (srvResult.Version.HasValue)
                    await notification.Wait(_timeout, srvResult.Version);

                _logger.LogDebug($"Role for user ({user.Id}) changed.");
            }

            return OperationResult.Success(user, roles);
        }

        public async Task<OperationResult> ChangeUsernameAsync(Guid userId, ChangeUsernameInputModel inputModel)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
                return OperationResult.Fail($"User ({userId}) not found.");
            
            if (!user.IsActive)
                return OperationResult.Fail($"User ({userId}) blocked.");
            
            if (user.UserName == DefaultAdminName)
                return OperationResult.Fail("Can't change super admin username.");

            var roles = await _userManager.GetRolesAsync(user);
            if (roles == null || !roles.Any())
                return OperationResult.Fail($"Roles for user ({userId}) not found.");

            user.UserName = inputModel.Username;

            var identityResult = await _userManager.UpdateAsync(user);
            if (!identityResult.Succeeded)
                return OperationResult.Fail(identityResult.GetErrors());
            
            await _signInManager.RefreshSignInAsync(user);
  
            var srvRequest = new ChangeUsernameRequest
            {
                UserId = userId.ToProto(),
                Username = inputModel.Username
            };

            using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
            {
                var srvResult = await _userClient.ChangeUsernameAsync(srvRequest);
                
                if (srvResult.Version.HasValue)
                    await notification.Wait(_timeout, srvResult.Version);
            }

            _logger.LogDebug($"Username for user ({user.Id}) changed.");
            
            return OperationResult.Success(user, roles);
        }

        public async Task<OperationResult> ChangeEmailAsync(Guid userId, ChangeEmailInputModel inputModel)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
                return OperationResult.Fail($"User ({userId}) not found.");
            
            if (!user.IsActive)
                return OperationResult.Fail($"User ({userId}) blocked.");

            var roles = await _userManager.GetRolesAsync(user);
            if (roles == null || !roles.Any())
                return OperationResult.Fail($"Roles for user ({userId}) not found.");

            var identityResult = await _userManager.SetEmailAsync(user, inputModel.Email);
            if (!identityResult.Succeeded)
            {
                if (user.Email != inputModel.Email)
                    return OperationResult.Fail(identityResult.GetErrors());
            }

            await _signInManager.RefreshSignInAsync(user);
            
            var srvRequest = new ChangeEmailRequest
            {
                UserId = userId.ToProto(),
                Email = inputModel.Email
            };
            
            using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
            {
                var srvResult = await _profileClient.ChangeEmailAsync(srvRequest);
                
                if (srvResult.Version.HasValue)
                    await notification.Wait(_timeout, srvResult.Version);
            }
            
            _logger.LogDebug($"Email for user ({user.Id}) changed.");
            
            return OperationResult.Success(user, roles);
        }

        public async Task<OperationResult> ActivateAsync(Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
                return OperationResult.Fail($"User ({userId}) not found.");

            if (user.UserName == DefaultAdminName)
                return OperationResult.Fail("Can't activate super admin.");
            
            var roles = await _userManager.GetRolesAsync(user);
            if (roles == null || !roles.Any())
                return OperationResult.Fail($"Roles for user ({userId}) not found.");
            
            user.IsActive = true;
            await _userManager.UpdateAsync(user);
            
            var srvRequest = new ActivateRequest {UserId = userId.ToProto()};
            
            using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
            {
                var srvResult = await _userClient.ActivateAsync(srvRequest);
                
                if (srvResult.Version.HasValue)
                    await notification.Wait(_timeout, srvResult.Version);
            }
            
            _logger.LogDebug($"User ({userId}) activated.");
            
            return OperationResult.Success(user, roles);
        }

        public async Task<OperationResult> BlockAsync(Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
                return OperationResult.Fail($"User ({userId}) not found.");

            if (user.UserName == DefaultAdminName)
                return OperationResult.Fail("Can't block super admin.");

            var roles = await _userManager.GetRolesAsync(user);
            if (roles == null || !roles.Any())
                return OperationResult.Fail($"Roles for user ({userId}) not found.");

            user.IsActive = false;
            await _userManager.UpdateAsync(user);
            
            var srvRequest = new BlockRequest {UserId = userId.ToProto()};
            
            using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
            {
                var srvResult = await _userClient.BlockAsync(srvRequest);
                
                if (srvResult.Version.HasValue)
                    await notification.Wait(_timeout, srvResult.Version);
            }
            
            _logger.LogDebug($"User ({userId}) blocked.");
            
            return OperationResult.Success(user, roles);
        }

        public async Task<OperationResult> RemoveAsync(Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
            {
                await RemoveFromServiceAsync(userId);
                return OperationResult.Fail($"User ({userId}) not found.");
            }
            
            if (user.UserName == DefaultAdminName)
                return OperationResult.Fail("Can't remove super admin.");
            
            var roles = await _userManager.GetRolesAsync(user);
            if (roles == null || !roles.Any())
                return OperationResult.Fail($"Roles for user ({userId}) not found.");
            
            await _userManager.DeleteAsync(user);
            
            await RemoveFromServiceAsync(userId);
  
            _logger.LogDebug($"User ({userId}) removed.");
            
            return OperationResult.Success(user, roles);
        }

        private async Task RemoveFromServiceAsync (Guid userId)
        {
            var srvRequest = new RemoveRequest {UserId = userId.ToProto()};
            
            using (var notification = await _notificationsChannel.StartListenAsync(_notificationContext.CurrentId))
            {
                var srvResult = await _userClient.RemoveAsync(srvRequest);
                
                if (srvResult.Version.HasValue)
                    await notification.Wait(_timeout, srvResult.Version);
            }
        }
    }
}