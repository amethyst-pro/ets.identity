using System;
using Amethyst.Grpc.Protobuf.Converters;
using Google.Protobuf;


namespace Ets.Identity.Services.Extensions
{
    public static class GuidExtensions
    {
        public static ByteString ToProto(this Guid guid)
            => GuidConverter.ToByteString(guid);

        public static Guid ToGuid(this ByteString proto)
            => GuidConverter.ToGuid(proto);
    }
}