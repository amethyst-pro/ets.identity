using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;

namespace Ets.Identity.Services.Extensions
{
    public static class IdentityResultExtensions
    {
        public static string GetErrors(this IdentityResult result)
            => result.Errors.Select(x => $"{x.Code}: {x.Description}").Join();
    }
}