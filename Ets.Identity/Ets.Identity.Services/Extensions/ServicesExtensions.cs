using Ets.Identity.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace Ets.Identity.Services.Extensions
{
    public static class ServicesExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
            => services.AddTransient<IAuthorizationService, AuthorizationService>()
                .AddTransient<IRegistrationService, RegistrationService>()
                .AddTransient<IUserService, UserService>();
    }
}