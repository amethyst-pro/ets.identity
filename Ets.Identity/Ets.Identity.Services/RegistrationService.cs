using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ets.Identity.Models.Entities;
using Ets.Identity.Models.InputModels;
using Ets.Identity.Models.Primitives;
using Ets.Identity.Models.ViewModels;
using Ets.Identity.Services.Contracts;
using Ets.Identity.Services.Extensions;
using Ets.Services.Users.Grpc;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace Ets.Identity.Services
{
    public sealed class RegistrationService : IRegistrationService
    {
        private readonly ILogger<RegistrationService> _logger;
        private readonly UserManager<DomainUser> _userManager;
        private readonly RoleManager<DomainRole> _roleManager;
        private readonly SignInManager<DomainUser> _signInManager;
        private readonly UserGrpc.UserGrpcClient _userClient;

        public RegistrationService(
            ILogger<RegistrationService> logger, 
            UserManager<DomainUser> userManager, 
            RoleManager<DomainRole> roleManager,
            SignInManager<DomainUser> signInManager, 
            UserGrpc.UserGrpcClient userClient)
        {
            _logger = logger;
            _userManager = userManager;
            _signInManager = signInManager;
            _userClient = userClient;
            _roleManager = roleManager;
        }

        public async Task<OperationResult> CreateUserAsync(CreateUserInputModel inputModel)
        {
            var user = new DomainUser
            {
                UserName = inputModel.Username,
                Email = inputModel.Email,
                IsActive = true
            };
            
            var isRoleExists = await _roleManager.RoleExistsAsync(inputModel.Role.ToString());
            if (!isRoleExists)
                return OperationResult.Fail($"Role ({inputModel.Role}) not exist.");
            
            var registrationResult = await _userManager.CreateAsync(user, inputModel.Password);
            if (!registrationResult.Succeeded)
            {  
                user = await _userManager.FindByNameAsync(inputModel.Username);
                if (user == null)
                    return OperationResult.Fail(registrationResult.GetErrors());
            }
            
            var roleResult = await _userManager.AddToRoleAsync(user, inputModel.Role.ToString());
            if (!roleResult.Succeeded)
            {
                if (!roleResult.Errors.Any(x => x.Code.Equals("UserAlreadyInRole")))
                    return OperationResult.Fail(roleResult.GetErrors());
            }

            await AddToUsersServiceAsync(user, inputModel.Role);

            await _signInManager.SignInAsync(user, true);
  
            _logger.LogDebug($"User ({user.Id}) registered.");
            
            if (!user.IsActive)
                return OperationResult.Fail($"User ({inputModel.Username}) blocked.");

            return OperationResult.Success(user, new List<string>{inputModel.Role.ToString()});
        }

        private async Task AddToUsersServiceAsync(DomainUser user, UserRole role)
        {
            var srvRequestCreate = new CreateRequest
            {
                UserId = user.Id.ToProto(),
                Username = user.UserName,
                Email = user.Email, 
                Role = (RoleProto)role
            };

            await _userClient.CreateAsync(srvRequestCreate);
        }
    }
}